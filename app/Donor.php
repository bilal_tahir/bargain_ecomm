<?php

namespace App;
use App\Blood;
use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
      public function blood(){

        return $this->hasMany(Blood::class , 'donor_id' );
    }
}
