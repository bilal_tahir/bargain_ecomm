<?php

namespace App;
use App\Donor;
use Illuminate\Database\Eloquent\Model;

class Blood extends Model
{
     public function donor(){

        return $this->belongsTo(Donor::class , 'donor_id' );
    }
}
