<?php

namespace App\Http\Controllers;

use App\Transfusion;
use App\Patient;
use App\BloodRecord;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransfusionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patient =  Patient::all();
        // dd($patient->patientbloodgroup);
        return view ('admin.patienttransfusion')
        ->with([
            'patient' => $patient,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $patient = Patient::find($request->patientid);
        $check = BloodRecord::where("BloodGroup",$patient->patientbloodgroup)->first();
        // dd($patient->patientbloodgroup);
        $bottlerecord = $check->BottlesAvailable;
        if($bottlerecord < $request->bottleneeded)
        {
          return redirect()->intended('admin')->withSuccess('Sorry! Your request did not submitted successfully.');
        }
        else
        {
            $trans = new Transfusion();
            $trans->patient_id = $request->patientid;
            $trans->bottle_taken = $request->bottleneeded;
            $trans->nexttransfusiondate = Carbon::now()->addMonths(1);
            $trans->save();
$delete = $request->bottleneeded;
             $update = BloodRecord::where("BloodGroup", $patient->patientbloodgroup)
                ->update(["BottlesAvailable" => $bottlerecord-$delete]);

                 return redirect()->intended('admin')->withSuccess('Sucess! Your request  submitted successfully.');  

        }
        // dd($check->BottlesAvailable);
        // dd($request->bottleneeded);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transfusion  $transfusion
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $patient = Patient::find($request->patientid);
        $check = BloodRecord::where("BloodGroup",$patient->patientbloodgroup)->first();
        $id = $request->patientid;
        // $bottle = $check->BottlesAvailable;
        return view ('admin.addtransfusion')->with([
            "check" => $check,
            "id" => $id,
        ]) ;
        // dd($check->BottlesAvailable);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transfusion  $transfusion
     * @return \Illuminate\Http\Response
     */
    public function edit(Transfusion $transfusion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transfusion  $transfusion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transfusion $transfusion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transfusion  $transfusion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transfusion $transfusion)
    {
        //
    }
}
