<?php

namespace App\Http\Controllers;

use App\Blood;
use App\Donor;
use Carbon\Carbon;
use App\BloodRecord;
use Illuminate\Http\Request;

class BloodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $donors =  Donor::all();
        return view ('admin.addblood')->with([
            'donors' => $donors,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
         $request->validate([
                'donorid' => 'required',
                'bottle' => 'required',

                
    ]);

    $group = Donor::find($request->donorid);
    $update = BloodRecord::where("BloodGroup", $group->donorbloodgroup)->first();
    $bottle = $update->BottlesAvailable;
    $add = $request->bottle;
    // dd($bottle+$add);
    $update = BloodRecord::where("BloodGroup", $group->donorbloodgroup)
    ->update(["BottlesAvailable" => $bottle+$add]);

// dd($update->BottlesAvailable);    

        $blood = new Blood();
        $blood->donor_id=$request->donorid;
        $blood->bottle=$request->bottle;
        $blood->expiry=Carbon::now()->addDays(5);
        $blood->nextdate=Carbon::now()->addMonths(1);
        $blood->save();

        return redirect()->intended('/addblood')->withSuccess('Great! Success.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blood  $blood
     * @return \Illuminate\Http\Response
     */
    public function show(Blood $blood)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blood  $blood
     * @return \Illuminate\Http\Response
     */
    public function edit(Blood $blood)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blood  $blood
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blood $blood)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blood  $blood
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blood $blood)
    {
        //
    }
    public function expiry()
    {
        $expiry = Blood::where('expiry','<',Carbon::now()->subDays(-2))->get();
        // dd($expiry);
        return view ('admin.expiry')->with([
            'expiry' => $expiry,
        ]);
    }

    public function bloodrecord()
    {
        $brecord = BloodRecord::all();
        return view ('admin.blood')->with([
            'record' => $brecord,
        ]);
    }
}
