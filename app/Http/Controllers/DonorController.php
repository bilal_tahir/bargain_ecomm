<?php

namespace App\Http\Controllers;

use App\Donor;
use App\Blood;
use File;
use Illuminate\Http\Request;

class DonorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    { 
   $files = $request->validate([
                'donorname' => 'required',
                
                'file'  => 'required',
                'donorbloodgroup' => 'required|max:2',
                'donornumber' => 'required|numeric',
                'donoraddress' => 'required',

                
    ]);


       $filenamewithext = $request->file->getClientOriginalName();

       $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);

       $extension = $request->file->getClientOriginalExtension();

       $filenametostore = $filename .'_'.time().'.'.$extension;
 
       $request->file->move(storage_path('resumes'), $filenametostore);

    //    dd($request->donorname);

    $donor = new Donor();
    $donor->donorname=$request->donorname;

    $donor->donorreport=$filenametostore;
    $donor->donorbloodgroup=$request->donorbloodgroup;
    $donor->donornumber=$request->donornumber;
    $donor->donoraddress=$request->donoraddress;
    

    $donor->save();

       return redirect()->intended('admin')->withSuccess('Great! Your request submitted successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Donor  $donor
     * @return \Illuminate\Http\Response
     */
    public function show(Donor $donor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Donor  $donor
     * @return \Illuminate\Http\Response
     */
    public function edit(Donor $donor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Donor  $donor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Donor $donor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Donor  $donor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Donor $donor)
    {
        //
    }

    public function donors(Request $request)
    {
       $record = Donor::find($request->id);
       $rec = $record->blood->count();
       if($rec>0)
       {
       $next = $record->blood[$rec-1];
       }
       else
       {
           $next = 0;
       }
    //    dd($next->nextdate);
    //    $rec = $record->blood;
    //    dd($rec);
       return view ('website.donorrecord')->with([
           'record'=>$record,
           'next'=>$next,
       ]);

    }

    public function donorpage()
    {
        return view ('website.donor');
    }

    public function registerdonor()
    {
        $list = Donor::all();
        return view ('admin.donorrecord')->with([
            'list' => $list,
        ]);
    }
    public function donorrecord()
    {
        $record = Donor::all();
        return view ('admin.recorddonor')->with([
            'record' => $record,
        ]);
    }

    public function report($id)
    {
        $report = Donor::where('id',$id)->first();
    //    $file = File::get(storage_path('resumes/'.$report->donorreport));
       return response()->file(storage_path('resumes/'.$report->donorreport));
    //    dd($file);
    }
}
