<?php

namespace App\Http\Controllers;

use App\Patient;
use App\BloodRecord;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
       $files = $request->validate([
                'patientname' => 'required',
                'patientbloodgroup' => 'required|max:2',
                'patientdisease' => 'required',


                
    ]);



    $patient = new Patient();
    $patient->patientname=$request->patientname;
    $patient->patientbloodgroup=$request->patientbloodgroup;
    $patient->patientdisease=$request->patientdisease;
 
    

    $patient->save();

       return redirect()->intended('admin')->withSuccess('Great! Your request submitted successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        //
    }

    public function patientpage()
    {
        $record = BloodRecord::all();
        return view ('website.patient')->with([
            "record" => $record,
        ]);
    }

    public function patient(Request $request)
    {
        $record = Patient::find($request->id);
       $rec = $record->transfusion->count();
    //    dd($record->donorname);
       if($rec>0)
       {
       $next = $record->transfusion[$rec-1];
       }
       else
       {
           $next = 0;
       }
    //  
       return view ('website.patientrecord')->with([
           'record'=>$record,
           'next'=>$next,
       ]);

    }

    public function patientrecord()
    {
        $record = Patient::all();
        return view ('admin.recordpatient')->with([
            'record' => $record,
        ]);
    }
}
