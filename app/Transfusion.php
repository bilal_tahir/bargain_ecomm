<?php

namespace App;
use App\Patient;
use Illuminate\Database\Eloquent\Model;

class Transfusion extends Model
{  public function patient(){

        return $this->belongsTo(Patient::class , 'patient_id' );
    }
}
