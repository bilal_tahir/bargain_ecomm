<?php

namespace App;
use App\Transfusion;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
      public function transfusion(){

        return $this->hasMany(Transfusion::class , 'patient_id' );
    }
}
