<?php

use Illuminate\Database\Seeder;
use App\BloodRecord;
class BloodRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       BloodRecord::create([
           'BloodGroup' => 'A+',
           'BottlesAvailable' => 0,
           
       ]);
       BloodRecord::create([
           'BloodGroup' => 'A-',
           'BottlesAvailable' => 0,
           
       ]);
       BloodRecord::create([
           'BloodGroup' => 'B+',
           'BottlesAvailable' => 0,
           
       ]);
       BloodRecord::create([
           'BloodGroup' => 'B-',
           'BottlesAvailable' => 0,
           
       ]);
       BloodRecord::create([
           'BloodGroup' => 'O+',
           'BottlesAvailable' => 0,
           
       ]);
       BloodRecord::create([
           'BloodGroup' => 'O-',
           'BottlesAvailable' => 0,
           
       ]);
       BloodRecord::create([
           'BloodGroup' => 'AB+',
           'BottlesAvailable' => 0,
           
       ]);
       BloodRecord::create([
           'BloodGroup' => 'AB-',
           'BottlesAvailable' => 0,
           
       ]);
    }
}
