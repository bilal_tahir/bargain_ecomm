<?php

use Illuminate\Database\Seeder;
use App\Patient;
class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Patient::create([
           'patientname' => 'Tesing Patient1',
           'patientbloodgroup' => 'A+',
           'patientdisease' => 'illness',
       ]);
        Patient::create([
           'patientname' => 'Tesing Patient2',
           'patientbloodgroup' => 'A-',
           'patientdisease' => 'illness',
       ]);
        Patient::create([
           'patientname' => 'Tesing Patient3',
           'patientbloodgroup' => 'O-',
           'patientdisease' => 'illness',
       ]);
        Patient::create([
           'patientname' => 'Tesing Patient4',
           'patientbloodgroup' => 'AB+',
           'patientdisease' => 'illness',
       ]);
    }
}
