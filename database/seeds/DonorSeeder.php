<?php

use Illuminate\Database\Seeder;
use App\Donor;
class DonorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Donor::create([
           'donorname' => 'Tesing Donor1',
           'donorreport' => 'phpheaders_1588256691.txt',
           'donorbloodgroup' => 'A+',
           'donornumber' => '1234567',
           'donoraddress' => 'Lahore',
       ]);

       Donor::create([
           'donorname' => 'Tesing Donor2',
           'donorreport' => 'phpheaders_1588256691.txt',
           'donorbloodgroup' => 'B+',
           'donornumber' => '6547632',
           'donoraddress' => 'Rawalpindi',
       ]);

       Donor::create([
           'donorname' => 'Tesing Donor3',
           'donorreport' => 'phpheaders_1588256691.txt',
           'donorbloodgroup' => 'O-',
           'donornumber' => '09876543',
           'donoraddress' => 'Karachi',
       ]);
    }
}
