<?php

use Illuminate\Database\Seeder;
use App\Blood;
class BloodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Blood::create([
           'donor_id' => '1',
           'bottle' => 'phpheaders_1588256691.txt',
           'nextdate' => 'A+',
       ]);
    }
}
