<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
         $this->call(DonorSeeder::class);
         $this->call(BloodRecordSeeder::class);
         $this->call(PatientSeeder::class);
    }
}
