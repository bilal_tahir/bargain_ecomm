<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransfusionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfusions', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->unsignedbigInteger('patient_id');
            $table->integer('bottle_taken');
            $table->string('nexttransfusiondate');
            $table->timestamps();

             $table->foreign('patient_id')->references('id')->on('patients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfusions');
    }
}
