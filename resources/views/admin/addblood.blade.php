  @extends('admin_layout.main')
 @section('content')
 
            
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                         @if ($message = Session::get('success'))
 
                <div class="alert alert-success alert-block">
 
                    <button type="button" class="close" data-dismiss="alert">×</button>
 
                    <strong>{{ $message }}</strong>
 
                </div>
            @endif
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">Add Blood</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Donor Module</h3>
                                        </div>
                                        <hr>
                                    <form action="/adddonorblood" method="post" enctype="multipart/form-data">
                                            @csrf
                                               <meta name="csrf-token" content="{{ csrf_token() }}">
                                            <div class="form-group">
                                                <label for="donorname" class="control-label mb-1">Donor Id</label>
                                                    <select name="donorid">
                                                        @foreach($donors as $id)
                                                    <option value="{{$id->id}}">{{$id->id}} {{$id->donorname}}</option>
                                                    @endforeach
                                                    </select>
                                               @if ($errors->has('donorid'))
                    <span class="text-danger">{{ $errors->first('donorid') }}</span>
                @endif
                                            </div>
                                            <div class="form-group has-success">
                                                <label for="bottle" class="control-label mb-1">No of Bottles</label>
                                                <input id="bottle" name="bottle" type="number" class="form-control">
                                                    @if ($errors->has('bottle'))
                    <span class="text-danger">{{ $errors->first('bottle') }}</span>
                @endif 
                                                </div>
                                                 <div>
                                                <button  type="submit" class="btn btn-lg btn-info btn-block">
                                                 Add
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



 @endsection