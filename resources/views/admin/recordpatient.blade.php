@extends('admin_layout.main')
 @section('content')
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>Patient ID</th>
                                                <th >Name</th>
                                                <th>Blood_Group</th>
                                                <th >Disease</th>
                                                <th >Registered_Day_Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($record as $records)
                                            <tr>
                                            <td>{{$records->id}}</td>
                                                <td>{{$records->patientname}}</td>
                                                <td >{{$records->patientbloodgroup}}</td>
                                                <td >{{$records->patientdisease}}</td>
                                                <td >{{$records->created_at}}</td>
                                           
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection