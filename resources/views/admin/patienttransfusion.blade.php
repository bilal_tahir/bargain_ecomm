  @extends('admin_layout.main')
 @section('content')
 
            
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                         @if ($message = Session::get('success'))
 
                <div class="alert alert-success alert-block">
 
                    <button type="button" class="close" data-dismiss="alert">×</button>
 
                    <strong>{{ $message }}</strong>
 
                </div>
            @endif
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">Patient Transfusion</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Patient Module</h3>
                                        </div>
                                        <hr>
                                    <form action="/addtransfusionrecord" method="post" enctype="multipart/form-data">
                                            @csrf
                                               <meta name="csrf-token" content="{{ csrf_token() }}">
                                            <div class="form-group">
                                                <label for="patientname" class="control-label mb-1">Patient Id</label>
                                                    <select name="patientid">
                                                        @foreach($patient as $id)
                                                    <option value="{{$id->id}}">{{$id->id}} {{$id->patientname}}</option>
                                                    @endforeach
                                                    </select>
                                               @if ($errors->has('patientid'))
                    <span class="text-danger">{{ $errors->first('patientid') }}</span>
                @endif
                                            </div>
                                            {{-- <div class="form-group has-success">
                                                <label for="bottle" class="control-label mb-1">Bottles Taken</label>
                                                <input id="bottle" name="bottle" type="number" class="form-control">
                                                    @if ($errors->has('bottle'))
                    <span class="text-danger">{{ $errors->first('bottle') }}</span>
                @endif 
                                                </div> --}}
                                                 <div>
                                                <button  type="submit" class="btn btn-lg btn-info btn-block">
                                                 Search Blood Bottles
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



 @endsection