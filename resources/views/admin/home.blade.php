  @extends('admin_layout.main')
 @section('content')
 
            
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                         @if ($message = Session::get('success'))
 
                <div class="alert alert-success alert-block">
 
                    <button type="button" class="close" data-dismiss="alert">×</button>
 
                    <strong>{{ $message }}</strong>
 
                </div>
            @endif
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">Register a New Donor</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Donor Module</h3>
                                        </div>
                                        <hr>
                                    <form action="/registerdonor" method="post" enctype="multipart/form-data">
                                            @csrf
                                               <meta name="csrf-token" content="{{ csrf_token() }}">
                                            <div class="form-group">
                                                <label for="donorname" class="control-label mb-1">Donor Name</label>
                                                <input id="donorname" name="donorname" type="text" class="form-control">
                                               @if ($errors->has('donorname'))
                    <span class="text-danger">{{ $errors->first('donorname') }}</span>
                @endif
                                            </div>
                                     
                                            <div class="form-group">
                                                <label for="donorreport" class="control-label mb-1">Upload Medical Report</label>
                                                <input id="donorreport" name="file" type="file" class="form-control">
                                                    @if ($errors->has('donorreport'))
                    <span class="text-danger">{{ $errors->first('donorreport') }}</span>
                @endif    
                                                </div>
                                             <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Blood Group</label>
                                                <input id="cc-number" name="donorbloodgroup" type="text" class="form-control">
                                                   @if ($errors->has('donorbloodgroup'))
                    <span class="text-danger">{{ $errors->first('donorbloodgroup') }}</span>
                @endif
                                            </div>
                                             <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Contact Number</label>
                                                <input id="cc-number" name="donornumber" type="text" class="form-control">
                                                   @if ($errors->has('donornumber'))
                    <span class="text-danger">{{ $errors->first('donornumber') }}</span>
                @endif
                                            </div>
                                             <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Address</label>
                                                <input id="cc-number" name="donoraddress" type="text" class="form-control">
                                                   @if ($errors->has('donoraddress'))
                    <span class="text-danger">{{ $errors->first('donoraddress') }}</span>
                @endif
                                            </div>
                                                 <div>
                                                <button  type="submit" class="btn btn-lg btn-info btn-block">
                                                    Register
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">Register a New Patient</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Patient Module</h3>
                                        </div>
                                        <hr>
                                    <form action="/registerpatient" method="post" enctype="multipart/form-data">
                                            @csrf
                                               <meta name="csrf-token" content="{{ csrf_token() }}">
                                            <div class="form-group">
                                                <label for="patientname" class="control-label mb-1">Patient Name</label>
                                                <input id="patientname" name="patientname" type="text" class="form-control">
                                               @if ($errors->has('patientname'))
                    <span class="text-danger">{{ $errors->first('patientname') }}</span>
                @endif
                                            </div>
                                     
                                             <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Blood Group</label>
                                                <input id="cc-number" name="patientbloodgroup" type="text" class="form-control">
                                                   @if ($errors->has('patientbloodgroup'))
                    <span class="text-danger">{{ $errors->first('patientbloodgroup') }}</span>
                @endif
                                            </div>
                                             <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Patient Disease</label>
                                                <input id="cc-number" name="patientdisease" type="text" class="form-control">
                                                   @if ($errors->has('patientdisease'))
                    <span class="text-danger">{{ $errors->first('patientdisease') }}</span>
                @endif
                                            </div>
                                           
                                                 <div>
                                                <button  type="submit" class="btn btn-lg btn-info btn-block">
                                                    Register Patient
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



 @endsection