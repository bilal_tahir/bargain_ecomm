@extends('admin_layout.main')
 @section('content')
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                              
                                                <th>Blood_Groups</th>
                                                <th>Bottles_Available</th>
                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($record as $records)
                                            <tr>
                                           
                                                <td>{{$records->BloodGroup}}</td>
                                                <td >{{$records->BottlesAvailable}}</td>
                                              
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection