@extends('admin_layout.main')
 @section('content')
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Donor ID</th>
                                                <th>Bottles</th>
                                                <th >Expiry</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($expiry as $records)
                                            <tr>
                                            <td>{{$records->id}}</td>
                                                <td>{{$records->donor_id}}</td>
                                                <td >{{$records->bottle}}</td>
                                                <td >{{$records->expiry}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection