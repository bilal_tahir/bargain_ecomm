  @extends('website_layout.main')
 @section('content')
 
	<!-- Menu -->

	<div class="menu_container menu_mm">

		<!-- Menu Close Button -->
		<div class="menu_close_container">
			<div class="menu_close"></div>
		</div>

		<!-- Menu Items -->
		<div class="menu_inner menu_mm">
			<div class="menu menu_mm">
				<ul class="menu_list menu_mm">
					<li class="menu_item menu_mm"><a href="index.html">Home</a></li>
					<li class="menu_item menu_mm"><a href="about.html">Blood Donor</a></li>
					<li class="menu_item menu_mm"><a href="services.html">Services</a></li>
					<li class="menu_item menu_mm"><a href="news.html">News</a></li>
					<li class="menu_item menu_mm"><a href="contact.html">Contact</a></li>
				</ul>
			</div>
			<div class="menu_extra">
				<div class="menu_appointment"><a href="#">Request an Appointment</a></div>
				<div class="menu_emergencies">For Emergencies: +563 47558 623</div>
			</div>

		</div>

	</div>
	
	<!-- Home -->

	<div class="home">
		<div class="home_slider_container">
			<!-- Home Slider -->
			<div class="owl-carousel owl-theme home_slider">
				
				<!-- Slider Item -->
				<div class="owl-item">
					<div class="home_slider_background" style="background-image:url(../assets/images/home_background_1.jpg)"></div>
					<div class="home_content">
						<div class="container">
							<div class="row">
								<div class="col">
									<div class="home_content_inner">
										<div class="home_title"><h1>Donate Today, You Never Know When you Need it!</h1></div>
										<div class="home_text">
											<p>Saving lives, uniting people and changing minds for healthy, safe and resilient communities.</p>
										</div>
										<div class="button home_button">
                                            
											<a href="#">read more</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Slider Item -->
				<div class="owl-item">
					<div class="home_slider_background" style="background-image:url(../assets/images/home_background_1.jpg)"></div>
					<div class="home_content">
						<div class="container">
							<div class="row">
								<div class="col">
									<div class="home_content_inner">
										<div class="home_title"><h1>Donate Today, You Never Know When you Need it!</h1></div>
										<div class="home_text">
											<p>Saving lives, uniting people and changing minds for healthy, safe and resilient communities.</p>
										</div>
										<div class="button home_button">
											<a href="#">read more</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Slider Item -->
				<div class="owl-item">
					<div class="home_slider_background" style="background-image:url(../assets/images/home_background_1.jpg)"></div>
					<div class="home_content">
						<div class="container">
							<div class="row">
								<div class="col">
									<div class="home_content_inner">
										<div class="home_title"><h1>Donate Today, You Never Know When you Need it!</h1></div>
										<div class="home_text">
											<p>Saving lives, uniting people and changing minds for healthy, safe and resilient communities.</p>
										</div>
										<div class="button home_button">
											<a href="#">read more</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<!-- Slider Progress -->
			<div class="home_slider_progress"></div>
		</div>
	</div>

 <div class="about">
 <div class="container">
        @if($record->count()>0)
        <h3>Patient Name</h3>
    <p>{{$record->patientname}}</p>
    <h3>Next Transfusion Date & Time</h3>
 @if($next != "0")
 <p >{{$next->nexttransfusiondate}}</p>
    @endif 
    @endif
        <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Donor Id</th>
      <th scope="col">Bottles Taken</th>
      <th scope="col">Date And Time</th>
      {{-- <th scope="col">Handle</th> --}}
    </tr>
  </thead>
  <tbody>
      @foreach($record->transfusion as $donor )
    <tr>
    <th >{{$donor->patient_id}}</th>
    <th >{{$donor->bottle_taken}}</th>
    <th >{{$donor->created_at}}</th>
      
    </tr>
 @endforeach
  </tbody>
</table>

    </div>
</div>

 @endsection