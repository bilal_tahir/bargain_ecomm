<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.home');
});

Route::get('/donors', 'DonorController@donorpage');
Route::post('/donorsrecord', 'DonorController@donors');
Route::get('/patients', 'PatientController@patientpage');
Route::post('/patientsrecord', 'PatientController@patient');


// Route::get('/addblood','BloodController@index');
// Route::get('/addtransfusion','TransfusionController@index');
// Route::get('/donorrecord','DonorController@donorrecord');
// Route::get('/patientrecord','PatientController@patientrecord');
// // Route::get('/registerdonor','DonorController@registerdonor');
// Route::post('/registerdonor','DonorController@create');
// Route::post('/adddonorblood','BloodController@create');
// Route::post('/registerpatient','PatientController@create');
// Route::post('/addtransfusionrecord','TransfusionController@show');
// Route::post('/transfusionrecord','TransfusionController@store');
// Route::get('/report/{id}','DonorController@report');
// Route::get('/expiry','BloodController@expiry');
// Route::get('/adminbloodrecord','BloodController@bloodrecord');

Route::group(['middleware' => ['auth' , 'admin']], function () {

    Route::get('/addblood','BloodController@index');
Route::get('/addtransfusion','TransfusionController@index');
Route::get('/donorrecord','DonorController@donorrecord');
Route::get('/patientrecord','PatientController@patientrecord');
// Route::get('/registerdonor','DonorController@registerdonor');
Route::post('/registerdonor','DonorController@create');
Route::post('/adddonorblood','BloodController@create');
Route::post('/registerpatient','PatientController@create');
Route::post('/addtransfusionrecord','TransfusionController@show');
Route::post('/transfusionrecord','TransfusionController@store');
Route::get('/report/{id}','DonorController@report');
Route::get('/expiry','BloodController@expiry');
Route::get('/adminbloodrecord','BloodController@bloodrecord');
Route::get('/admin', function () {
    return view('admin.home');
});

});

// Route::get('/','WebsiteController@home') 

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
